package danga.makeabike.listView;

/**
 * Created by dan on 6/20/2015.
 */

        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import danga.makeabike.R;

public class ListViewAdapter extends BaseAdapter {

    // Declare Variables
    Context context;
    String[] titles;
    String[] texts;
    int[] images;
    LayoutInflater inflater;

    public ListViewAdapter(Context context, String[] titles, String[] texts,  int[] images) {

        this.context = context;
        this.titles = titles;
        this.texts = texts;
        this.images = images;
    }

    public int getCount() {
        return titles.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // Declare Variables
        TextView txtrank;
        TextView txtcountry;
        ImageView imgflag;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.list_item, parent, false);

        // Locate the TextViews in listview_item.xml
        txtrank = (TextView) itemView.findViewById(R.id.title);
        txtcountry = (TextView) itemView.findViewById(R.id.text);

        // Locate the ImageView in listview_item.xml
        imgflag = (ImageView) itemView.findViewById(R.id.icon);

        // Capture position and set to the TextViews
        txtrank.setText(titles[position]);
        txtcountry.setText(texts[position]);

        // Capture position and set to the ImageView
        imgflag.setImageResource(images[position]);

        return itemView;
    }
}