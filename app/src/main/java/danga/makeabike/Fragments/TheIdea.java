package danga.makeabike.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import danga.makeabike.listView.ListViewAdapter;
import danga.makeabike.R;
import danga.makeabike.listView.SingleItemView;

/**
 * Created by dan on 6/20/2015.
 */
public class TheIdea  extends android.support.v4.app.Fragment {

    String[] titles;
    String[] texts;
    int[] images;
    ListView list;
    ListViewAdapter adapter;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.the_idea_layout, container, false);

        perform();

        return rootView;
    }

    public void perform() {
        // Generate sample data

        titles = new String[] {"4 simple steps and your new bike is on the way...", "1) Assemble the bike", "2) Seal the deal", "3) Wait for the call", "4) Ride like the wind" };

        String openingStr = "Have you always wished you had a way to assemble parts of your choice, and create the ultimate bicycle for you? Sounds complex? don't worry, its as easy as pie!";
        String AssembleTheBikeStr = "In the \"make a bike\" tab, you will find an easy-to-use interface that will walk you through the assembly of your bike step by step.";
        String SealTheDealStr = "In the \"cart\" tab you can see the bike models you have assembled, and (if you wish), order them. We offer many ways of payment for your convenience.";
        String WaitForTheCallStr = "Once all of the parts are in our warehouse, our professional bicycle engineers will assemble and test your bike. When this is done, you will be notified both by phone and by E-mail.";
        String RideLikeTheWindStr = "Once your bike is ready, you can go hit the roads. We will still provide you with full tech support for the whole first year.\n" + "Godspeed :)";
        texts = new String[] { openingStr, AssembleTheBikeStr, SealTheDealStr, WaitForTheCallStr, RideLikeTheWindStr };

        images = new int[] { R.drawable.thinking, R.drawable.bicycle_parts, R.drawable.row_of_bicycles, R.drawable.wait_for_the_call, R.drawable.ride_like_the_wind };

        // Locate the ListView in fragmenttab1.xml
        list = (ListView) rootView.findViewById(R.id.theIdea_list);

        // Pass results to ListViewAdapter Class
        adapter = new ListViewAdapter(getActivity(), titles, texts, images);

        // Binds the Adapter to the ListView
        list.setAdapter(adapter);

       /* // Capture clicks on ListView items
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Send single item click data to SingleItemView Class
                Intent i = new Intent(getActivity(), SingleItemView.class);
                // Pass all data rank
                i.putExtra("rank", titles);
                // Pass all data country
                i.putExtra("country", texts);
                // Pass all data population
                i.putExtra("population", population);
                // Pass all data flag
                i.putExtra("flag", images);
                // Pass a single position
                i.putExtra("position", position);
                // Open SingleItemView.java Activity
                startActivity(i);
            }

        });*/
    }
}
