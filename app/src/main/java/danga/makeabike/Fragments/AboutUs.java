package danga.makeabike.Fragments;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import danga.makeabike.R;

/**
 * Created by dan on 6/20/2015.
 */
public class AboutUs extends android.support.v4.app.Fragment {

    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.about_us_layout, container, false);


        //fields

        ImageView IVus = (ImageView) rootView.findViewById(R.id.imgUs);
        TextView TVtitle = (TextView) rootView.findViewById(R.id.title);
        TextView TVbody = (TextView) rootView.findViewById(R.id.body);


        //image
        AnimationDrawable animation = new AnimationDrawable();
        animation.addFrame(getResources().getDrawable(R.drawable.dan), 5000);
        animation.addFrame(getResources().getDrawable(R.drawable.ido), 5000);
        animation.addFrame(getResources().getDrawable(R.drawable.lior), 5000);
        animation.setOneShot(false);
        //title and body
        String title = "Wanna learn a little about the people behind the curtain ?";
        String body = "MakeBike is a company founded by ben-gurion university students, in 2015. The idea was to let bicycle enthusiasts build their own custom made ride.We have a huge variety of the best parts in the world, in an affordable price.The idea of the company was to improve the buying experience. The bicycle industry in israel is based mostly on big stores that both have a limited variety of products, and are not very flexible in assembling a custom bike.The founders of the company - Dan gadasi, Ido basch, and Lior Nardea, are software engineering students, are all bicycle fans, and the idea came out of their own experience in the sport. We would be happy to make your dream bike come to life..";

        //set and start image us animation
        IVus.setBackgroundDrawable(animation);
        animation.start();
        //set the text view and the image view
        TVtitle.setText(title);
        TVbody.setText(body);


        return rootView;
    }

}
