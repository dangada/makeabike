package danga.makeabike.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import danga.makeabike.R;
import danga.makeabike.listView.ListViewAdapter;

/**
 * Created by dan on 6/20/2015.
 */
public class BuildYourBike  extends android.support.v4.app.Fragment {

    String[] titles;
    String[] texts;
    int[] images;
    ListView list;
    ListViewAdapter adapter;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.build_your_bike_layout, container, false);
        perform();
        return rootView;
    }


    public void perform() {
        // Generate sample data

        titles = new String[]{"Start Building your bike:","1) Frame" , "2) Wheels", "3) Shifters", "4) Seat", "5) Brakes", "6) Handle Bar",};

        String openStr = "This is a step-by-step assembly page for your bike.\n" + "Follow the stage numbers to add parts in the correct order.;";
        texts = new String[]{openStr," ", " ", " ", " ", " ", " "};

        images = new int[]{R.drawable.build_bike, R.drawable.bike_frame, R.drawable.bike_wheels, R.drawable.bike_shifters, R.drawable.bike_seat,R.drawable.bike_brakes, R.drawable.bike_handleb_bar};

        // Locate the ListView in fragmenttab1.xml
        list = (ListView) rootView.findViewById(R.id.buildYourBike_list);

        // Pass results to ListViewAdapter Class
        adapter = new ListViewAdapter(getActivity(), titles, texts, images);

        // Binds the Adapter to the ListView
        list.setAdapter(adapter);
    }
}