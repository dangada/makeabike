package danga.makeabike.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import danga.makeabike.R;

/**
 * Created by dan on 6/20/2015.
 */
public class Home extends android.support.v4.app.Fragment {

    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.home_layout, container, false);
        return rootView;
    }
}
